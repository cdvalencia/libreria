'use strict';

var app = require('../..');
import request from 'supertest';

var newAuthor;

describe('Author API:', function() {

  describe('GET /api/authors', function() {
    var authors;

    beforeEach(function(done) {
      request(app)
        .get('/api/authors')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          authors = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      authors.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/authors', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/authors')
        .send({
          name: 'New Author',
          info: 'This is the brand new author!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newAuthor = res.body;
          done();
        });
    });

    it('should respond with the newly created author', function() {
      newAuthor.name.should.equal('New Author');
      newAuthor.info.should.equal('This is the brand new author!!!');
    });

  });

  describe('GET /api/authors/:id', function() {
    var author;

    beforeEach(function(done) {
      request(app)
        .get('/api/authors/' + newAuthor._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          author = res.body;
          done();
        });
    });

    afterEach(function() {
      author = {};
    });

    it('should respond with the requested author', function() {
      author.name.should.equal('New Author');
      author.info.should.equal('This is the brand new author!!!');
    });

  });

  describe('PUT /api/authors/:id', function() {
    var updatedAuthor;

    beforeEach(function(done) {
      request(app)
        .put('/api/authors/' + newAuthor._id)
        .send({
          name: 'Updated Author',
          info: 'This is the updated author!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedAuthor = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAuthor = {};
    });

    it('should respond with the updated author', function() {
      updatedAuthor.name.should.equal('Updated Author');
      updatedAuthor.info.should.equal('This is the updated author!!!');
    });

  });

  describe('DELETE /api/authors/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/authors/' + newAuthor._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when author does not exist', function(done) {
      request(app)
        .delete('/api/authors/' + newAuthor._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
