'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var booksCtrlStub = {
  index: 'booksCtrl.index',
  show: 'booksCtrl.show',
  create: 'booksCtrl.create',
  update: 'booksCtrl.update',
  destroy: 'booksCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var booksIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './books.controller': booksCtrlStub
});

describe('Books API Router:', function() {

  it('should return an express router instance', function() {
    booksIndex.should.equal(routerStub);
  });

  describe('GET /api/bookss', function() {

    it('should route to books.controller.index', function() {
      routerStub.get
        .withArgs('/', 'booksCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/bookss/:id', function() {

    it('should route to books.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'booksCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/bookss', function() {

    it('should route to books.controller.create', function() {
      routerStub.post
        .withArgs('/', 'booksCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/bookss/:id', function() {

    it('should route to books.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'booksCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/bookss/:id', function() {

    it('should route to books.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'booksCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/bookss/:id', function() {

    it('should route to books.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'booksCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
