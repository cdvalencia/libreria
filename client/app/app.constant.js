(function(angular, undefined) {
'use strict';

angular.module('libreriaApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);