'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://admin:Admin123@ds017862.mlab.com:17862/libreria'
  },

  // Seed database on startup
  seedDB: true

};
