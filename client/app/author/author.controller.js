'use strict';

angular.module('libreriaApp')
  .controller('AuthorCtrl', function ($scope,services) {
    

    $scope.authors=[];
    $scope.update=true;

    services.authors().async().then(function(d){
        $scope.authors=d.data;
        console.log(d.data)
    });

    $scope.enviar=function(){
        var newAuthor={
            name:$scope.name,
            nationality:$scope.nation
        }
        console.log(newAuthor)
        services.newAuthor(newAuthor).async().then(function(d){
            $scope.authors.push(d.data)            
        });
    }

    $scope.deleteAuthor=function(id,index){
        services.deleteAuthor(id).async().then(function(d){
            $scope.authors.splice(index,1)
        });
    }

    $scope.verAuthor=function(index){
        $scope.update=false;
        console.log($scope.authors[index])
        $scope.UpdateName=$scope.authors[index].name;
        $scope.updateNation=$scope.authors[index].nationality;
        

    }

    $scope.regresar=function(){
        $scope.update=true;
    }

  });
