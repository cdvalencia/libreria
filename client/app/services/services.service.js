'use strict';

angular.module('libreriaApp')
  .service('services', function ($http) {

    // SERVICES FOR BOOKS

        var books = function() {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.get('/api/books')
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

        var newBook = function(item) {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.post('/api/books',item)
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

        var deleteBook = function(item) {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.delete('/api/books/'+item)
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

    // END SERVICES FOR BOOKS


    // SERVICES FOR AUTHORS

        var authors = function() {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.get('/api/authors')
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

        var newAuthor = function(item) {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.post('/api/authors',item)
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

        var deleteAuthor = function(item) {
          var data = {
            async: function () {
              // $http returns a promise, which has a then function, which also returns a promise
              var promise = $http.delete('/api/authors/'+item)
                .success(function (response) {
                  return response;

                });
              // Return the promise to the controller
              return promise;
            }
          };
          return data;
        };

    // END SERVICES FOR AUTHORS



    return {
      books:books,
      newBook:newBook,
      deleteBook:deleteBook,
      authors:authors,
      newAuthor:newAuthor,
      deleteAuthor:deleteAuthor,
    }
  });
