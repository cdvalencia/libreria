'use strict';

angular.module('libreriaApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('authors', {
        url: '/authors',
        templateUrl: 'app/author/authors.html',
        controller: 'AuthorCtrl'
      })
  });
