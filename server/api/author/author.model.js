'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var AuthorSchema = new mongoose.Schema({
  name: String,
  nationality: String
});

export default mongoose.model('Author', AuthorSchema);
