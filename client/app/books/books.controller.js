'use strict';

angular.module('libreriaApp')
  .controller('BooksCtrl', function ($scope,services) {
    

    $scope.books=[];
    $scope.update=true;

    services.books().async().then(function(d){
        $scope.books=d.data;
        console.log(d.data)
    });

    $scope.selected='';
    services.authors().async().then(function(d){
        $scope.authors=d.data        
          
    });

    $scope.enviar=function(){

        var author=JSON.parse($scope.selected)
        var newBook={
            title:$scope.book,
            description:$scope.description,
            image:"image",
            author:author._id
        }
        console.log(newBook)

        services.newBook(newBook).async().then(function(d){
            $scope.books.push(d.data)            
        });
    }

    $scope.deleteBook=function(id,index){
        services.deleteBook(id).async().then(function(d){
            $scope.books.splice(index,1)
        });
    }

    $scope.verBook=function(index){
        $scope.update=false;
        $scope.bookUpdate=$scope.books[index].title;
        $scope.descriptionUpdate=$scope.books[index].description;
        $scope.authorUpdate=$scope.books[index].author;

    }

    $scope.regresar=function(){
        $scope.update=true;
    }

    

  });
