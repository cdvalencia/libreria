'use strict';

angular.module('libreriaApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('book', {
        url: '/books',
        templateUrl: 'app/books/books.html',
        controller: 'BooksCtrl'
      })
  });
