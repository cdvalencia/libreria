'use strict';

var app = require('../..');
import request from 'supertest';

var newBooks;

describe('Books API:', function() {

  describe('GET /api/bookss', function() {
    var bookss;

    beforeEach(function(done) {
      request(app)
        .get('/api/bookss')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          bookss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      bookss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/bookss', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/bookss')
        .send({
          name: 'New Books',
          info: 'This is the brand new books!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newBooks = res.body;
          done();
        });
    });

    it('should respond with the newly created books', function() {
      newBooks.name.should.equal('New Books');
      newBooks.info.should.equal('This is the brand new books!!!');
    });

  });

  describe('GET /api/bookss/:id', function() {
    var books;

    beforeEach(function(done) {
      request(app)
        .get('/api/bookss/' + newBooks._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          books = res.body;
          done();
        });
    });

    afterEach(function() {
      books = {};
    });

    it('should respond with the requested books', function() {
      books.name.should.equal('New Books');
      books.info.should.equal('This is the brand new books!!!');
    });

  });

  describe('PUT /api/bookss/:id', function() {
    var updatedBooks;

    beforeEach(function(done) {
      request(app)
        .put('/api/bookss/' + newBooks._id)
        .send({
          name: 'Updated Books',
          info: 'This is the updated books!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedBooks = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBooks = {};
    });

    it('should respond with the updated books', function() {
      updatedBooks.name.should.equal('Updated Books');
      updatedBooks.info.should.equal('This is the updated books!!!');
    });

  });

  describe('DELETE /api/bookss/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/bookss/' + newBooks._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when books does not exist', function(done) {
      request(app)
        .delete('/api/bookss/' + newBooks._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
