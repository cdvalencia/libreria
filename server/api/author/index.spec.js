'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var authorCtrlStub = {
  index: 'authorCtrl.index',
  show: 'authorCtrl.show',
  create: 'authorCtrl.create',
  update: 'authorCtrl.update',
  destroy: 'authorCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var authorIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './author.controller': authorCtrlStub
});

describe('Author API Router:', function() {

  it('should return an express router instance', function() {
    authorIndex.should.equal(routerStub);
  });

  describe('GET /api/authors', function() {

    it('should route to author.controller.index', function() {
      routerStub.get
        .withArgs('/', 'authorCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/authors/:id', function() {

    it('should route to author.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'authorCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/authors', function() {

    it('should route to author.controller.create', function() {
      routerStub.post
        .withArgs('/', 'authorCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/authors/:id', function() {

    it('should route to author.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'authorCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/authors/:id', function() {

    it('should route to author.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'authorCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/authors/:id', function() {

    it('should route to author.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'authorCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
