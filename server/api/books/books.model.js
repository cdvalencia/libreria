'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var BooksSchema = new mongoose.Schema({
  title: String,
  description: String,
  image: String,
  Author: String
});

export default mongoose.model('Books', BooksSchema);
