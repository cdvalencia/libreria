'use strict';

angular.module('libreriaApp', [
  'libreriaApp.auth',
  'libreriaApp.admin',
  'libreriaApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'validation.match'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/books');

    $locationProvider.html5Mode(true);
  });
